class UsersController < ApplicationController

  def index
    # Generate the user with the counter the it has
    render json: User.new.generate_user
  end

  # Handles the manual user creation
  def create
    if Campaign.new.value.blank?
      redirect_to campaigns_path, flash: { danger: "First there has to be a json file define" }
    else
      # Set the params from the view
      user = User.new(name: params[:user]["name"], a: params[:user]["a"], b: params[:user]["b"], c: params[:user]["c"])

      generated_user = @user.generate_manual_user(user)
      campaigns = Campaign.new.value

      # Launch the matching with the campaigns in memory and the user pass in the params
      User.new.targeted_user(campaigns, generated_user)

      # Go and see the result of this manual search
      redirect_to last_auto_search_path
    end
  rescue
    redirect_to import_camp_path, flash: { danger: "You have to choose a valid json file" }
  end
end
