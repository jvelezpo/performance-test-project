class CampaignsController < ApplicationController

  # Sets the variables to use in the view
  def index
    @campaign = Campaign.new
  end

  # Handles the importation of the campaigns json file
  def create
    uploaded_file = params[:file]

    # Set the Json file in the server memory
    @campaign = Campaign.new.set_json_file(uploaded_file)

    redirect_to import_camp_path
  rescue
    redirect_to import_camp_path, flash: { danger: "You have to choose a valid json file" }
  end

end
