class SearchController < ApplicationController

  # Sets the variables to use in the view
  def index
      @user = User.new

      @campaigns = Campaign.new.value
  end

  # Does the matching with the campaign file in memory with the user store in the db
  # process it and then returns a json value to the view
  def auto_search

    # If there is no campaigns json file return the user to a path where the user can upload one
    if Campaign.new.value.blank?
      redirect_to campaigns_path, flash: { danger: "First there has to be a json file define" }
    else
      campaigns = Campaign.new.value
      user = User.first.generate_user
      highest_price = User.new.targeted_user(campaigns, user)

      # If the highest price is null then show 'none' otherwise show the highest price campaign
      render json: highest_price['campaign_name'].nil? ? 'none' : highest_price['campaign_name']
    end
  end

  # Sets the variables to the view of last used values and results
  def last_auto_search
    campaign = Campaign.new

    @used_user = campaign.used_user
    @used_campaigns = campaign.used_campaigns
    @highest_price = campaign.highest_price
    @targeted_user = campaign.targeted_user
    @last_diff = campaign.last_diff
  end

end
