class Campaign
  include ActiveModel::Model

  # This are the campaigns that are uploaded by the user
  @@loaded_campaigns = {}

  ######################################################################
  # This variables are use every time a matching (user-campaigns) is run
  # The objective of this is store the last result
  # so it can be show to the user interested
  # to know ho the last result when
  ######################################################################
  @@used_user = {}
  @@used_campaigns = []
  @@highest_price = {}
  @@targeted_user = []
  @@last_diff = nil

  ###################
  # Setters & Getters
  ###################
  def value
    @@loaded_campaigns
  end

  def value=(value)
    @@loaded_campaigns = value
  end

  def used_user
    @@used_user
  end

  def used_user=(value)
    @@used_user = value
  end

  def used_campaigns
    @@used_campaigns
  end

  def used_campaigns=(value)
    @@used_campaigns = value
  end

  def highest_price
    @@highest_price
  end

  def highest_price=(value)
    @@highest_price = value
  end

  def targeted_user
    @@targeted_user
  end

  def targeted_user=(value)
    @@targeted_user = value
  end

  def last_diff
    @@last_diff
  end

  def last_diff=(value)
    @@last_diff = value
  end

  # Set the Json file in the server memory
  def set_json_file(file)
    file_content = file.read

    hash = JSON.parse file_content

    self.value = hash
  end

end