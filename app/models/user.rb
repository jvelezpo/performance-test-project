class User < ActiveRecord::Base

  # Variables of instance, these are not store in DB, they are here to help in the manual search
  attr_accessor :name, :a, :b, :c

  # Define the array of letters we are going to use in the generation of attributes for each user
  ALPHABET = ("A".."Z").to_a

  # It generates automatically an user with all its attributes,
  # according to the counter of request to the generate user API
  def generate_user

    user = User.first

    # If the user does not exist then create a new one
    if user.blank?
      user = User.create!(counter: 0)
    end

    # Increment the counter for every request made to the api
    count = User.increment_counter(:counter, user)

    # Set this class varible update with the value in DB
    user.counter += count

    {
        user: "u#{user.counter}",
        profile: generate_profile(user.counter)
    }
  end

  # Generate the profile attr base on the alphabet length
  def generate_profile(counter)
    profiles = counter % 26
    if profiles == 0
      if counter > 0
        profiles = 26
      else
        profiles = 1
      end
    end

    attributes = Hash.new

    (1..profiles).map do |i|
      attributes["attr_#{User::ALPHABET[i-1]}"] = "#{User::ALPHABET[i-1]}#{rand(1..200)}"
    end

    # Return the profile attributes
    attributes
  end

  # Generates a user base on whet the user passes as parameters
  def generate_manual_user(user)
    {
        user: user.name,
        profile: generate_manual_profile(user)
    }
  end

  # Very small profile hash, just to show how it works the manual user creation
  def generate_manual_profile(user)
    profile = {}
    profile["attr_A"] = user.a if !user.a.blank?
    profile["attr_B"] = user.b if !user.b.blank?
    profile["attr_C"] = user.c if !user.c.blank?
    profile
  end

  # If all targets of the campaign can be found in a user's profile, then this user is called “Targeted User”.
  def targeted_user(campaigns, user)
    # Start taking time 
    start = Time.now

    targeted_user = []
    highest_price = {}

    campaigns.each do |campaign|

      is_it_targeted = is_it_targeted?(campaign, user)

      if is_it_targeted
        targeted_user << campaign

        if highest_price.blank?
          highest_price = campaign
        else
          highest_price = campaign if campaign["price"] > highest_price["price"]
        end
      end
    end

    finish = Time.now
    diff = ((finish - start)*1000).round(5)

    campaign = Campaign.new

    campaign.used_user = user
    campaign.used_campaigns = campaigns
    campaign.highest_price = highest_price
    campaign.targeted_user = targeted_user
    campaign.last_diff = diff

    # Return targeted_users
    highest_price
  end

  # Checks individual campaigns if all targets of the campaign can be found in an user's profile
  # and the user’s profile attribute value can be found in the list of the campaign target attr_list
  def is_it_targeted?(campaign, user)
    is_it_targeted = true
    campaign["target_list"].each do |target|
      target_attr =  target["target"]
      if user[:profile][target_attr].nil?
        is_it_targeted = false
        break
      end

      user_max_target = user[:profile][target_attr][1, user[:profile][target_attr].size-1].to_i
      campaign_max_target = target["attr_list"].last[1, target["attr_list"].last.size-1].to_i

      if user_max_target > campaign_max_target
        is_it_targeted = false
        break
      end
    end
    is_it_targeted
  end

end
