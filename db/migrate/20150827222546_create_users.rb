class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.integer :counter

      t.timestamps null: false
    end
  end
end
